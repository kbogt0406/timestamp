
-- Testbench automatically generated online
-- at http://vhdl.lapinoo.net
-- Generation date : 28.6.2018 09:33:21 GMT

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity tb_strigger is
generic(data_width : integer :=16);
end tb_strigger;

architecture tb of tb_strigger is

    component strigger
        port (clk       : in std_logic;
              en        : in std_logic;
              rst       : in std_logic;
              din       : in std_logic_vector (data_width-1 downto 0);
              threshold : in unsigned ((data_width/2)-1 downto 0);
              trigger   : out std_logic);
    end component;

    signal clk       : std_logic;
    signal en        : std_logic;
    signal rst       : std_logic;
    signal din       : std_logic_vector (data_width-1 downto 0);
    signal threshold : unsigned ((data_width/2)-1 downto 0);
    signal trigger   : std_logic;

    constant TbPeriod : time := 4 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : strigger
    port map (clk       => clk,
              en        => en,
              rst       => rst,
              din       => din,
              threshold => threshold,
              trigger   => trigger);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        din <= (others => '0');
        threshold <= (others => '0');

        -- Reset generation
        -- EDIT: Check that rst is really your reset signal
        rst <= '0';
        en<='0';
        wait for 100 ns;
        rst <= '1';
        wait for 100 ns;
        threshold<=to_unsigned(50,8);
        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;
        wait for 2 ns;

        -- Stop the clock and hence terminate the simulation
        for I in 0 to 65535 loop
           din<=std_logic_vector(unsigned(din)+1);
           wait for 4ns;
           if I = 2500 then
            threshold<=to_unsigned(200,8);
            elsif I=500 then
            en<='1';
            end if;
        end loop;
        
        wait;
    end process;

end tb;
