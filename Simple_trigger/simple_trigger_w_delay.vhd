----------------------------------------------------------------------------------
-- Company: ICTP
-- Engineer: LUIS GARCIA
--
-- Create Date: 04/25/2018 09:38:46 AM
-- Design Name: TIMESTAMP
-- Module Name: STRIGER - Behavioral
-- Project Name: TIMESTAMP
-- Target Devices:
-- Tool Versions:
-- Description:
-- This block will create a diferential trigger for the high speed INFN High speed ADC without decimation
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments: TIMESTAMP WITH TRIGGER OCCURANCE
--Latency observed is of 1 clk cicle
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shift_trigger is
  generic(data_width : integer :=16;
          n_width : integer :=8
  );
  port(
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;

    en : IN STD_LOGIC;

    din : IN STD_LOGIC_VECTOR(data_width-1 downto 0);

    threshold : IN Unsigned((data_width/2)-1 downto 0);

    trigger : OUT STD_LOGIC;

    n : in unsigned(n_width -1 downto 0)


  );

end shift_trigger;

architecture Behavioral of shift_trigger is
  signal d0, dn : signed(data_width/2 downto 0);
  signal thr_sig : Unsigned(data_width/2 -1 downto 0);
--  signal diff : unsigned(data_width/2 downto 0);
  signal trig_sig : std_logic;

  type shift_reg is array ((2**(n_width-1))-1 downto 0) of STD_LOGIC_VECTOR(data_width-1 downto 0); --shift register stores two values at a time.
  signal buff : shift_reg;
  signal m, sb :integer;
  signal n_signal : unsigned(n_width -1 downto 0);

begin
--  m<=0 when rst='0' else to_integer(n_signal)/2;
--  sb<=0 when rst='0' else to_integer(n_signal) mod 2;

--  d0<=(others=>'0') when rst='0' else 
--      signed('0' & unsigned(buff(0)((data_width/2) -1 downto 0))); --D0 the most recent data input
  
--  dn<=(others=>'0') when rst='0' else 
--      signed('0' & unsigned(buff(m)(((data_width/2)*(1+sb))-1 downto ((data_width/2)*sb)))); --defining the m value of buffer if its the MSB or the LSB


  process (clk,rst, threshold, d0, dn)
  begin
    if rst = '0' then
      thr_sig<=threshold;
      trig_sig<='0';
      n_signal<=(others=>'0');
    elsif rising_edge(clk) then
      n_signal<=n;
      thr_sig<=threshold;
      if unsigned(abs(dn-d0))>=thr_sig then
        trig_sig<='1';
      else
        trig_sig<='0';
      end if;
    end if;
  end process;

  process(clk, rst) --shift register process
  begin
    if rst='0' then
      buff<=(others=>(others=>'0'));
    elsif rising_edge(clk) then
      buff(0)<=din;
      for i in 0 to (2**(n_width-1))-2 loop
        buff(i+1)<=buff(i);
      end loop;
    end if;
  end process;
  
  process(clk, rst, buff) --shift register process
  begin
    if rst='0' then
      d0<=(others=>'0');
      dn<=(others=>'0');
      m<=0;
      sb<=0;
    elsif rising_edge(clk) then
      m<=to_integer(n_signal)/2;
      sb<=to_integer(n_signal) mod 2;
      d0<=signed('0' & unsigned(buff(0)((data_width/2) -1 downto 0)));
      dn<=signed('0' & unsigned(buff(m)(((data_width/2)*(1+sb))-1 downto ((data_width/2)*sb))));
--      diff<=unsigned(abs(signed('0' & unsigned(buff(m)(((data_width/2)*(1+sb))-1 downto ((data_width/2)*sb))))-signed('0' & unsigned(buff(0)((data_width/2) -1 downto 0)))));
    end if;
  end process;

trigger<=trig_sig and en;

end Behavioral;
