-- Testbench automatically generated online
-- at http://vhdl.lapinoo.net
-- Generation date : 12.9.2018 14:42:57 GMT

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity tb_FIFOMGR is
  generic(fifo_din_width : integer :=32;
          din_width  : integer :=16;
          delay    : integer :=3;
          fifo_depth : integer := 1536
          );
end tb_FIFOMGR;

architecture tb of tb_FIFOMGR is

  component sinewave is
  port (clk :in  std_logic;
        rst :in  std_logic;
        dataout : out integer range -128 to 127;
        dout : out std_logic_vector(15 downto 0)
        );
  end component;

    component FIFOMGR
  generic(fifo_din_width : integer :=32;
            din_width  : integer :=16;
            delay    : integer :=3;
            fifo_depth : integer := 1536
            );
        port (CLK         : in std_logic;
              RST         : in std_logic;
              EN          : in std_logic;
              CL          : in std_logic;
              SB          : in unsigned (7 downto 0);
              SA          : in unsigned (7 downto 0);
              TS          : in std_logic;
              LT          : in std_logic;
              TSS         : in std_logic_vector (fifo_din_width-1 downto 0);
              TSF         : in std_logic_vector (fifo_din_width-1 downto 0);
              ADC_DIN     : in std_logic_vector (din_width-1 downto 0);
              FIFO_AFULL  : in std_logic;
              FIFO_FULL   : in std_logic;
              FIFO_AEMPTY : in std_logic;
              FIFO_EMPTY  : in std_logic;
              FIFO_DATA   : out std_logic_vector (fifo_din_width-1 downto 0);
              FIFO_WE     : out std_logic;
              FIFO_CLR    : out std_logic);
    end component;

    signal CLK         : std_logic;
    signal RST         : std_logic;
    signal EN          : std_logic;
    signal CL          : std_logic;
    signal SB          : unsigned (7 downto 0);
    signal SA          : unsigned (7 downto 0);
    signal TS          : std_logic;
    signal LT          : std_logic;
    signal TSS         : std_logic_vector (fifo_din_width-1 downto 0);
    signal TSF         : std_logic_vector (fifo_din_width-1 downto 0);
--    signal ADC_DIN     : std_logic_vector (din_width-1 downto 0);
    signal FIFO_AFULL  : std_logic;
    signal FIFO_FULL   : std_logic;
    signal FIFO_AEMPTY : std_logic;
    signal FIFO_EMPTY  : std_logic;
    signal FIFO_DATA   : std_logic_vector (fifo_din_width-1 downto 0);
    signal FIFO_WE     : std_logic;
    signal FIFO_CLR    : std_logic;

    constant TbPeriod : time := 4 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';
    
    signal sine_data : std_logic_vector(15 downto 0);

begin

      sigGen : sinewave
      port map (clk =>clk,
                rst =>rst,
                dataout => open,
                dout => sine_data
                );

    dut : FIFOMGR
    port map (CLK         => CLK,
              RST         => RST,
              EN          => EN,
              CL          => CL,
              SB          => SB,
              SA          => SA,
              TS          => TS,
              LT          => LT,
              TSS         => TSS,
              TSF         => TSF,
              ADC_DIN     => sine_data,
              FIFO_AFULL  => FIFO_AFULL,
              FIFO_FULL   => FIFO_FULL,
              FIFO_AEMPTY => FIFO_AEMPTY,
              FIFO_EMPTY  => FIFO_EMPTY,
              FIFO_DATA   => FIFO_DATA,
              FIFO_WE     => FIFO_WE,
              FIFO_CLR    => FIFO_CLR);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        EN <= '0';
        CL <= '0';
        SB <= (others => '0');
        SA <= (others => '0');
        TS <= '0';
        LT <= '0';
        TSS <= (others => '0');
        TSF <= (others => '0');

        FIFO_AFULL <= '0';
        FIFO_FULL <= '0';
        FIFO_AEMPTY <= '0';
        FIFO_EMPTY <= '0';

        -- Reset generation
        -- EDIT: Check that RST is really your reset signal
        RST <= '0';
        wait for 14 ns;
        RST <= '1';
        wait for 12 ns;

        SB<=x"05";
        SA<=x"08";

        TSS<=x"12345678";
        TSF<=x"87654321";
        TS<='1';
        wait for 4 ns;
        TS<='0';
        
        wait for 16 ns;
        EN<='1';
        TS<='1';
        WAIT FOR 4 ns;
        TS<='0';

        -- EDIT Add stimuli here
--        wait for 100 * TbPeriod;

--        -- Stop the clock and hence terminate the simulation
--        TbSimEnded <= '1';
        wait;
    end process;

end tb;
