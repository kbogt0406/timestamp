--Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
--Date        : Tue Jul 24 15:27:55 2018
--Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
--Command     : generate_target design_2.bd
--Design      : design_2
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_2 is
  port (
    CLK_IN1_D_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_IN1_D_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    Ctrl_reg_in_0 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Ctrl_reg_out_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    adc_raw : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_pl : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_adc_DS_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_adc_DS_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_wiz_resetn : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_from_adc_DS_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_from_adc_DS_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ext_reset_in : in STD_LOGIC;
    from_adc_or_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    from_adc_or_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    rtc_rst : out STD_LOGIC_VECTOR ( 0 to 0 );
    sys_clock : in STD_LOGIC;
    x_from_adc_calrun_fmc : in STD_LOGIC;
    x_to_adc_cal_fmc : out STD_LOGIC;
    x_to_adc_caldly_nscs_fmc : out STD_LOGIC;
    x_to_adc_dclk_rst_fmc : out STD_LOGIC;
    x_to_adc_fsr_ece_fmc : out STD_LOGIC;
    x_to_adc_led_0 : out STD_LOGIC;
    x_to_adc_led_1 : out STD_LOGIC;
    x_to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    x_to_adc_outv_slck_fmc : out STD_LOGIC;
    x_to_adc_pd_fmc : out STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of design_2 : entity is "design_2,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_2,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=7,numReposBlks=7,numNonXlnxBlks=1,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,da_axi4_cnt=10,da_board_cnt=4,synth_mode=Global}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of design_2 : entity is "design_2.hwdef";
end design_2;

architecture STRUCTURE of design_2 is
  component design_2_util_ds_buf_0_1 is
  port (
    IBUF_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_OUT : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_2_util_ds_buf_0_1;
  component design_2_clk_wiz_0_0 is
  port (
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    clk_out1 : out STD_LOGIC
  );
  end component design_2_clk_wiz_0_0;
  component design_2_util_ds_buf_0_0 is
  port (
    OBUF_IN : in STD_LOGIC_VECTOR ( 0 to 0 );
    OBUF_DS_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    OBUF_DS_N : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_2_util_ds_buf_0_0;
  component design_2_util_ds_buf_0_2 is
  port (
    IBUF_DS_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    IBUF_DS_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    IBUF_OUT : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component design_2_util_ds_buf_0_2;
  component design_2_util_ds_buf_1_0 is
  port (
    IBUF_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_OUT : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_2_util_ds_buf_1_0;
  component design_2_proc_sys_reset_1_0 is
  port (
    slowest_sync_clk : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    mb_debug_sys_rst : in STD_LOGIC;
    dcm_locked : in STD_LOGIC;
    mb_reset : out STD_LOGIC;
    bus_struct_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    interconnect_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_2_proc_sys_reset_1_0;
  component design_2_fmc_adc_controller_1_0 is
  port (
    Ctrl_reg_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Ctrl_reg_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    x_to_adc_cal_fmc : out STD_LOGIC;
    x_to_adc_caldly_nscs_fmc : out STD_LOGIC;
    x_to_adc_fsr_ece_fmc : out STD_LOGIC;
    x_to_adc_outv_slck_fmc : out STD_LOGIC;
    x_to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    x_to_adc_dclk_rst_fmc : out STD_LOGIC;
    x_to_adc_pd_fmc : out STD_LOGIC;
    x_to_adc_led_0 : out STD_LOGIC;
    x_to_adc_led_1 : out STD_LOGIC;
    x_from_adc_calrun_fmc : in STD_LOGIC;
    x_from_adc_or : in STD_LOGIC
  );
  end component design_2_fmc_adc_controller_1_0;
  signal CLK_IN1_D_clk_n_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal CLK_IN1_D_clk_p_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Ctrl_reg_in_0_1 : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal Net : STD_LOGIC_VECTOR ( 0 to 0 );
  signal clk_wiz_0_clk_out1 : STD_LOGIC;
  signal clk_wiz_resetn_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal data_from_adc_DS_N_1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal data_from_adc_DS_P_1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal ext_reset_in_1 : STD_LOGIC;
  signal fmc_adc_controller_1_Ctrl_reg_out : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal fmc_adc_controller_1_x_to_adc_cal_fmc : STD_LOGIC;
  signal fmc_adc_controller_1_x_to_adc_caldly_nscs_fmc : STD_LOGIC;
  signal fmc_adc_controller_1_x_to_adc_dclk_rst_fmc : STD_LOGIC;
  signal fmc_adc_controller_1_x_to_adc_fsr_ece_fmc : STD_LOGIC;
  signal fmc_adc_controller_1_x_to_adc_led_0 : STD_LOGIC;
  signal fmc_adc_controller_1_x_to_adc_led_1 : STD_LOGIC;
  signal fmc_adc_controller_1_x_to_adc_outedge_ddr_sdata_fmc : STD_LOGIC;
  signal fmc_adc_controller_1_x_to_adc_outv_slck_fmc : STD_LOGIC;
  signal fmc_adc_controller_1_x_to_adc_pd_fmc : STD_LOGIC;
  signal from_adc_or_DS_N_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal from_adc_or_DS_P_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal proc_sys_reset_1_peripheral_aresetn : STD_LOGIC_VECTOR ( 0 to 0 );
  signal sys_clock_1 : STD_LOGIC;
  signal util_ds_buf_0_OBUF_DS_N : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_ds_buf_0_OBUF_DS_P : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_ds_buf_2_IBUF_OUT : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal util_ds_buf_3_IBUF_OUT : STD_LOGIC_VECTOR ( 0 to 0 );
  signal x_from_adc_calrun_fmc_1 : STD_LOGIC;
  signal NLW_proc_sys_reset_1_mb_reset_UNCONNECTED : STD_LOGIC;
  signal NLW_proc_sys_reset_1_bus_struct_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_1_interconnect_aresetn_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_1_peripheral_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of ext_reset_in : signal is "xilinx.com:signal:reset:1.0 RST.EXT_RESET_IN RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of ext_reset_in : signal is "XIL_INTERFACENAME RST.EXT_RESET_IN, POLARITY ACTIVE_HIGH";
  attribute X_INTERFACE_INFO of sys_clock : signal is "xilinx.com:signal:clock:1.0 CLK.SYS_CLOCK CLK";
  attribute X_INTERFACE_PARAMETER of sys_clock : signal is "XIL_INTERFACENAME CLK.SYS_CLOCK, CLK_DOMAIN design_2_sys_clock, FREQ_HZ 100000000, PHASE 0.000";
  attribute X_INTERFACE_INFO of clk_wiz_resetn : signal is "xilinx.com:signal:reset:1.0 RST.CLK_WIZ_RESETN RST";
  attribute X_INTERFACE_PARAMETER of clk_wiz_resetn : signal is "XIL_INTERFACENAME RST.CLK_WIZ_RESETN, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of data_from_adc_DS_N : signal is "xilinx.com:signal:data:1.0 DATA.DATA_FROM_ADC_DS_N DATA";
  attribute X_INTERFACE_PARAMETER of data_from_adc_DS_N : signal is "XIL_INTERFACENAME DATA.DATA_FROM_ADC_DS_N, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of data_from_adc_DS_P : signal is "xilinx.com:signal:data:1.0 DATA.DATA_FROM_ADC_DS_P DATA";
  attribute X_INTERFACE_PARAMETER of data_from_adc_DS_P : signal is "XIL_INTERFACENAME DATA.DATA_FROM_ADC_DS_P, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of rtc_rst : signal is "xilinx.com:signal:reset:1.0 RST.RTC_RST RST";
  attribute X_INTERFACE_PARAMETER of rtc_rst : signal is "XIL_INTERFACENAME RST.RTC_RST, POLARITY ACTIVE_LOW";
begin
  CLK_IN1_D_clk_n_1(0) <= CLK_IN1_D_clk_n(0);
  CLK_IN1_D_clk_p_1(0) <= CLK_IN1_D_clk_p(0);
  Ctrl_reg_in_0_1(8 downto 0) <= Ctrl_reg_in_0(8 downto 0);
  Ctrl_reg_out_0(1 downto 0) <= fmc_adc_controller_1_Ctrl_reg_out(1 downto 0);
  adc_raw(15 downto 0) <= util_ds_buf_2_IBUF_OUT(15 downto 0);
  clk_pl(0) <= Net(0);
  clk_to_adc_DS_N(0) <= util_ds_buf_0_OBUF_DS_N(0);
  clk_to_adc_DS_P(0) <= util_ds_buf_0_OBUF_DS_P(0);
  clk_wiz_resetn_1(0) <= clk_wiz_resetn(0);
  data_from_adc_DS_N_1(15 downto 0) <= data_from_adc_DS_N(15 downto 0);
  data_from_adc_DS_P_1(15 downto 0) <= data_from_adc_DS_P(15 downto 0);
  ext_reset_in_1 <= ext_reset_in;
  from_adc_or_DS_N_1(0) <= from_adc_or_DS_N(0);
  from_adc_or_DS_P_1(0) <= from_adc_or_DS_P(0);
  rtc_rst(0) <= proc_sys_reset_1_peripheral_aresetn(0);
  sys_clock_1 <= sys_clock;
  x_from_adc_calrun_fmc_1 <= x_from_adc_calrun_fmc;
  x_to_adc_cal_fmc <= fmc_adc_controller_1_x_to_adc_cal_fmc;
  x_to_adc_caldly_nscs_fmc <= fmc_adc_controller_1_x_to_adc_caldly_nscs_fmc;
  x_to_adc_dclk_rst_fmc <= fmc_adc_controller_1_x_to_adc_dclk_rst_fmc;
  x_to_adc_fsr_ece_fmc <= fmc_adc_controller_1_x_to_adc_fsr_ece_fmc;
  x_to_adc_led_0 <= fmc_adc_controller_1_x_to_adc_led_0;
  x_to_adc_led_1 <= fmc_adc_controller_1_x_to_adc_led_1;
  x_to_adc_outedge_ddr_sdata_fmc <= fmc_adc_controller_1_x_to_adc_outedge_ddr_sdata_fmc;
  x_to_adc_outv_slck_fmc <= fmc_adc_controller_1_x_to_adc_outv_slck_fmc;
  x_to_adc_pd_fmc <= fmc_adc_controller_1_x_to_adc_pd_fmc;
clk_pl_RnM: component design_2_util_ds_buf_0_1
     port map (
      IBUF_DS_N(0) => CLK_IN1_D_clk_n_1(0),
      IBUF_DS_P(0) => CLK_IN1_D_clk_p_1(0),
      IBUF_OUT(0) => Net(0)
    );
clk_wiz_0: component design_2_clk_wiz_0_0
     port map (
      clk_in1 => sys_clock_1,
      clk_out1 => clk_wiz_0_clk_out1,
      resetn => clk_wiz_resetn_1(0)
    );
fmc_adc_controller_1: component design_2_fmc_adc_controller_1_0
     port map (
      Ctrl_reg_in(8 downto 0) => Ctrl_reg_in_0_1(8 downto 0),
      Ctrl_reg_out(1 downto 0) => fmc_adc_controller_1_Ctrl_reg_out(1 downto 0),
      x_from_adc_calrun_fmc => x_from_adc_calrun_fmc_1,
      x_from_adc_or => util_ds_buf_3_IBUF_OUT(0),
      x_to_adc_cal_fmc => fmc_adc_controller_1_x_to_adc_cal_fmc,
      x_to_adc_caldly_nscs_fmc => fmc_adc_controller_1_x_to_adc_caldly_nscs_fmc,
      x_to_adc_dclk_rst_fmc => fmc_adc_controller_1_x_to_adc_dclk_rst_fmc,
      x_to_adc_fsr_ece_fmc => fmc_adc_controller_1_x_to_adc_fsr_ece_fmc,
      x_to_adc_led_0 => fmc_adc_controller_1_x_to_adc_led_0,
      x_to_adc_led_1 => fmc_adc_controller_1_x_to_adc_led_1,
      x_to_adc_outedge_ddr_sdata_fmc => fmc_adc_controller_1_x_to_adc_outedge_ddr_sdata_fmc,
      x_to_adc_outv_slck_fmc => fmc_adc_controller_1_x_to_adc_outv_slck_fmc,
      x_to_adc_pd_fmc => fmc_adc_controller_1_x_to_adc_pd_fmc
    );
proc_sys_reset_1: component design_2_proc_sys_reset_1_0
     port map (
      aux_reset_in => '1',
      bus_struct_reset(0) => NLW_proc_sys_reset_1_bus_struct_reset_UNCONNECTED(0),
      dcm_locked => '1',
      ext_reset_in => ext_reset_in_1,
      interconnect_aresetn(0) => NLW_proc_sys_reset_1_interconnect_aresetn_UNCONNECTED(0),
      mb_debug_sys_rst => '0',
      mb_reset => NLW_proc_sys_reset_1_mb_reset_UNCONNECTED,
      peripheral_aresetn(0) => proc_sys_reset_1_peripheral_aresetn(0),
      peripheral_reset(0) => NLW_proc_sys_reset_1_peripheral_reset_UNCONNECTED(0),
      slowest_sync_clk => Net(0)
    );
util_ds_buf_0: component design_2_util_ds_buf_0_0
     port map (
      OBUF_DS_N(0) => util_ds_buf_0_OBUF_DS_N(0),
      OBUF_DS_P(0) => util_ds_buf_0_OBUF_DS_P(0),
      OBUF_IN(0) => clk_wiz_0_clk_out1
    );
util_ds_buf_2: component design_2_util_ds_buf_0_2
     port map (
      IBUF_DS_N(15 downto 0) => data_from_adc_DS_N_1(15 downto 0),
      IBUF_DS_P(15 downto 0) => data_from_adc_DS_P_1(15 downto 0),
      IBUF_OUT(15 downto 0) => util_ds_buf_2_IBUF_OUT(15 downto 0)
    );
util_ds_buf_3: component design_2_util_ds_buf_1_0
     port map (
      IBUF_DS_N(0) => from_adc_or_DS_N_1(0),
      IBUF_DS_P(0) => from_adc_or_DS_P_1(0),
      IBUF_OUT(0) => util_ds_buf_3_IBUF_OUT(0)
    );
end STRUCTURE;
