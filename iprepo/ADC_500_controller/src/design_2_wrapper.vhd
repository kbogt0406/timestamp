--Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
--Date        : Tue Jul 24 15:27:55 2018
--Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
--Command     : generate_target design_2_wrapper.bd
--Design      : design_2_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_2_wrapper is
  port (
    CLK_IN1_D_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_IN1_D_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    Ctrl_reg_in_0 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Ctrl_reg_out_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    adc_raw : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_pl : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_adc_DS_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_adc_DS_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_wiz_resetn : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_from_adc_DS_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_from_adc_DS_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ext_reset_in : in STD_LOGIC;
    from_adc_or_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    from_adc_or_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    rtc_rst : out STD_LOGIC_VECTOR ( 0 to 0 );
    sys_clock : in STD_LOGIC;
    x_from_adc_calrun_fmc : in STD_LOGIC;
    x_to_adc_cal_fmc : out STD_LOGIC;
    x_to_adc_caldly_nscs_fmc : out STD_LOGIC;
    x_to_adc_dclk_rst_fmc : out STD_LOGIC;
    x_to_adc_fsr_ece_fmc : out STD_LOGIC;
    x_to_adc_led_0 : out STD_LOGIC;
    x_to_adc_led_1 : out STD_LOGIC;
    x_to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    x_to_adc_outv_slck_fmc : out STD_LOGIC;
    x_to_adc_pd_fmc : out STD_LOGIC
  );
end design_2_wrapper;

architecture STRUCTURE of design_2_wrapper is
  component design_2 is
  port (
    x_to_adc_led_0 : out STD_LOGIC;
    from_adc_or_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    from_adc_or_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    x_to_adc_led_1 : out STD_LOGIC;
    x_to_adc_dclk_rst_fmc : out STD_LOGIC;
    x_from_adc_calrun_fmc : in STD_LOGIC;
    rtc_rst : out STD_LOGIC_VECTOR ( 0 to 0 );
    sys_clock : in STD_LOGIC;
    x_to_adc_cal_fmc : out STD_LOGIC;
    x_to_adc_pd_fmc : out STD_LOGIC;
    x_to_adc_caldly_nscs_fmc : out STD_LOGIC;
    x_to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    data_from_adc_DS_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    CLK_IN1_D_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_adc_DS_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_IN1_D_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_from_adc_DS_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_to_adc_DS_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    x_to_adc_fsr_ece_fmc : out STD_LOGIC;
    x_to_adc_outv_slck_fmc : out STD_LOGIC;
    Ctrl_reg_in_0 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    adc_raw : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ext_reset_in : in STD_LOGIC;
    clk_wiz_resetn : in STD_LOGIC_VECTOR ( 0 to 0 );
    Ctrl_reg_out_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    clk_pl : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_2;
begin
design_2_i: component design_2
     port map (
      CLK_IN1_D_clk_n(0) => CLK_IN1_D_clk_n(0),
      CLK_IN1_D_clk_p(0) => CLK_IN1_D_clk_p(0),
      Ctrl_reg_in_0(8 downto 0) => Ctrl_reg_in_0(8 downto 0),
      Ctrl_reg_out_0(1 downto 0) => Ctrl_reg_out_0(1 downto 0),
      adc_raw(15 downto 0) => adc_raw(15 downto 0),
      clk_pl(0) => clk_pl(0),
      clk_to_adc_DS_N(0) => clk_to_adc_DS_N(0),
      clk_to_adc_DS_P(0) => clk_to_adc_DS_P(0),
      clk_wiz_resetn(0) => clk_wiz_resetn(0),
      data_from_adc_DS_N(15 downto 0) => data_from_adc_DS_N(15 downto 0),
      data_from_adc_DS_P(15 downto 0) => data_from_adc_DS_P(15 downto 0),
      ext_reset_in => ext_reset_in,
      from_adc_or_DS_N(0) => from_adc_or_DS_N(0),
      from_adc_or_DS_P(0) => from_adc_or_DS_P(0),
      rtc_rst(0) => rtc_rst(0),
      sys_clock => sys_clock,
      x_from_adc_calrun_fmc => x_from_adc_calrun_fmc,
      x_to_adc_cal_fmc => x_to_adc_cal_fmc,
      x_to_adc_caldly_nscs_fmc => x_to_adc_caldly_nscs_fmc,
      x_to_adc_dclk_rst_fmc => x_to_adc_dclk_rst_fmc,
      x_to_adc_fsr_ece_fmc => x_to_adc_fsr_ece_fmc,
      x_to_adc_led_0 => x_to_adc_led_0,
      x_to_adc_led_1 => x_to_adc_led_1,
      x_to_adc_outedge_ddr_sdata_fmc => x_to_adc_outedge_ddr_sdata_fmc,
      x_to_adc_outv_slck_fmc => x_to_adc_outv_slck_fmc,
      x_to_adc_pd_fmc => x_to_adc_pd_fmc
    );
end STRUCTURE;
