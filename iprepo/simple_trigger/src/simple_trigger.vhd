----------------------------------------------------------------------------------
-- Company: ICTP
-- Engineer: LUIS GARCIA
--
-- Create Date: 04/25/2018 09:38:46 AM
-- Design Name: TIMESTAMP
-- Module Name: STRIGER - Behavioral
-- Project Name: TIMESTAMP
-- Target Devices:
-- Tool Versions:
-- Description:
-- This block will create a diferential trigger for the high speed INFN High speed ADC without decimation
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments: TIMESTAMP WITH TRIGGER OCCURANCE
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity strigger is
  generic(data_width : integer :=16);
  port(
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    
    en : IN STD_LOGIC;

    din : IN STD_LOGIC_VECTOR(data_width-1 downto 0);

    threshold : IN Unsigned((data_width/2)-1 downto 0);

    trigger : OUT STD_LOGIC


  );

end strigger;

architecture Behavioral of strigger is
  signal d0, d1 : signed(data_width/2 downto 0);
  signal thr_sig : Unsigned(data_width/2 -1 downto 0);
  signal trig_sig : std_logic;
begin
  d0<=signed('0' & unsigned(din((data_width/2) -1 downto 0)));
  d1<=signed('0' & unsigned(din(data_width-1 downto (data_width/2)))); --SPLITTING DATA COMMING FROM ADC

--  trig_sig<= '1' when unsigned(abs(d1-d0))>=thr_sig else '0';

  

  process (clk,rst, threshold, d0, d1)
  begin
    if rst = '0' then
      thr_sig<=threshold;
      trig_sig<='0';
    elsif rising_edge(clk) then
      thr_sig<=threshold;
      if unsigned(abs(d1-d0))>=thr_sig then
        trig_sig<='1';
      else
        trig_sig<='0';
      end if;
    end if;
  end process;

trigger<=trig_sig and en;
end Behavioral;
