----------------------------------------------------------------------------------
-- Company: ICTP
-- Engineer: LUIS GARCIA
--
-- Create Date: 04/25/2018 09:38:46 AM
-- Design Name: TIMESTAMP
-- Module Name: STRIGER - Behavioral
-- Project Name: TIMESTAMP
-- Target Devices:
-- Tool Versions:
-- Description:
-- This block will create a diferential trigger for the high speed INFN High speed ADC without decimation
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments: TIMESTAMP WITH TRIGGER OCCURANCE
--Latency observed is of 1 clk cicle
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shift_trigger is
  generic(data_width : integer :=16;
          n_width : integer :=8
  );
  port(
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;

    en : IN STD_LOGIC;

    din : IN STD_LOGIC_VECTOR(data_width-1 downto 0);

    threshold : IN Unsigned((data_width/2)-1 downto 0);

    trigger : OUT STD_LOGIC;

    n : in unsigned(n_width -1 downto 0)


  );

end shift_trigger;

architecture Behavioral of shift_trigger is
  constant buff_size : integer :=2**n_width;
  signal d0, d1 : signed(data_width/2 downto 0);
  type shift_reg is array (buff_size-1 downto 0) of signed(data_width/2 downto 0);
  signal buff : shift_reg;

  signal trig_sig : STD_LOGIC;


begin
  d0<=signed('0' & unsigned(din(data_width/2-1 downto 0)));
  d1<=signed('0' & unsigned(din(data_width-1 downto data_width/2)));

  process(clk, rst) --process in charge of shift register
  begin
    if rst='0' then
      buff<=(others=>(others=>'0'));
    elsif rising_edge(clk) then
      for i in 0 to buff_size/2-2 loop --buff_size-1 <= 2*i+3
        buff(2*i+2)<=buff(2*i); --shift pair values
        buff(2*i+3)<=buff(2*i+1); --shift odd values
      end loop;
      buff(0)<=d0;
      buff(1)<=d1;
    end if;
  end process;

  process (clk,rst, threshold, buff, n) --process in charge of comparing threshold
  begin
    if rst = '0' then
      trig_sig<='0';
    elsif rising_edge(clk) then
      if unsigned(abs(buff(to_integer(n))-buff(0)))>=('0'&threshold) then
        trig_sig<='1';
      else
        trig_sig<='0';
      end if;
    end if;
  end process;

trigger<=trig_sig and en;

end Behavioral;
