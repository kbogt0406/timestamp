----------------------------------------------------------------------------------
-- Company: ICTP
-- Engineer: LUIS GARCIA
--
-- Create Date: 04/25/2018 09:38:46 AM
-- Design Name: TIMESTAMP
-- Module Name: STRIGER - Behavioral
-- Project Name: TIMESTAMP
-- Target Devices:
-- Tool Versions:
-- Description:
-- This block will create a diferential trigger for the high speed INFN High speed ADC without decimation
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments: TIMESTAMP WITH TRIGGER OCCURANCE
--Latency observed is of 1 clk cicle
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ram_shift_trigger is
  generic(data_width : integer :=16;
          n_width : integer :=9
  );
  port(
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;

    en : IN STD_LOGIC;

    din : IN STD_LOGIC_VECTOR(data_width-1 downto 0);

    threshold : IN Unsigned((data_width/2)-1 downto 0);

    trigger : OUT STD_LOGIC;

    n : in std_logic_vector(n_width -1 downto 0)


  );

end ram_shift_trigger;

architecture Behavioral of ram_shift_trigger is

COMPONENT c_shift_ram_0
  PORT (
    A : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    D : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    CLK : IN STD_LOGIC;
    Q : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

  signal d0, dn : signed(data_width/2 downto 0);
  signal Q : std_logic_vector(data_width-1 downto 0);

  
  signal trig_sig : STD_LOGIC;

begin
  shift: c_shift_ram_0
  PORT MAP (
    A => n(n_width-1 downto 1),
    D => din,
    CLK => clk,
    Q => Q
  );
  
  d0<=signed('0' & unsigned(din(data_width/2-1 downto 0)));
  dn<=signed('0' & unsigned(Q(data_width-1 downto data_width/2))) when n(0)='1' else signed('0' & unsigned(Q(data_width/2-1 downto 0))) when n(0)='0';
  
process (clk,rst, threshold, d0, dn) --process in charge of comparing threshold
  begin
    if rst = '0' then
      trig_sig<='0';
    elsif rising_edge(clk) then
      if unsigned(abs(dn-d0)) >=  ('0'&threshold) then
        trig_sig<='1';
      else
        trig_sig<='0';
      end if;
    end if;
  end process;
  

trigger<=trig_sig and en;


end Behavioral;
