-- Testbench automatically generated online
-- at http://vhdl.lapinoo.net
-- Generation date : 27.8.2018 10:54:26 GMT

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity tb_mstrigger is
  generic(data_width : integer :=16);
end tb_mstrigger;

architecture tb of tb_mstrigger is

  component sinewave is
  port (clk :in  std_logic;
        rst :in  std_logic;
        dataout : out integer range -128 to 127;
        dout : out std_logic_vector(15 downto 0)
        );
  end component;

    component mstrigger
        port (clk       : in std_logic;
              rst       : in std_logic;
              din       : in std_logic_vector (data_width-1 downto 0);
              en        : in std_logic;
              threshold : in unsigned (data_width/2-1 downto 0);
              Lat       : out std_logic;
              TS        : out std_logic);
    end component;

    signal clk       : std_logic;
    signal rst       : std_logic;
    signal din       : std_logic_vector (data_width-1 downto 0);
    signal en        : std_logic;
    signal threshold : unsigned (data_width/2-1 downto 0);
    signal TS        : std_logic;
    signal LAT       : std_logic;

    constant TbPeriod : time := 2 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';
    signal sine_data : std_logic_vector(15 downto 0);

begin

    dut : mstrigger
    port map (clk       => clk,
              rst       => rst,
              din       => din,
              en        => en,
              threshold => threshold,
              LAT => LAT,
              TS        => TS);

    sigGen : sinewave
    port map (clk =>clk,
              rst =>rst,
              dataout => open,
              dout => sine_data
              );

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        din <= (others => '0');
        en <= '0';
        threshold <= (others => '0');


        -- Reset generation
        -- EDIT: Check that rst is really your reset signal
        rst <= '0';
        wait for 10 ns;
        rst <= '1';
        wait for 11 ns;

        threshold <= "00010000"; --16
        din<=sine_data;
        en<='1';

        -- for i in 0 to 255 loop
        --   din<=std_logic_vector(to_unsigned(i,8))&std_logic_vector(to_unsigned(255-i,8));
        --   wait for TbPeriod;
        -- end loop;
        for i in 0 to 10000 loop
          din<=sine_data;
          wait for TbPeriod;
        end loop;

        -- EDIT Add stimuli here
        -- wait for 100 * TbPeriod;


        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;
